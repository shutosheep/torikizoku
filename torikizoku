#!/bin/sh
# SPDX-FileCopyrightText: 2024 Shuto Tamaoka
# SPDX-License-Identifier: MIT

TMP_FILE="/tmp/torikizoku.tmp"
CACHE_DIR="$HOME/.cache/torikizoku"
BASE_URL="https://torikizoku.co.jp/menu"
CURL_OPTS="-sf"

dep_check() {
    for c; do
        command -v $c > /dev/null 2>&1 || (echo "torikizoku: command not found: $c" 1>&2 ; return 1)
    done
}

cache() {
    dep_check "curl" || exit 1

    echo "Caching html files to $CACHE_DIR ..." 1>&2

    rm -f ${CACHE_DIR}/*.html
    [ -d "$CACHE_DIR" ] || mkdir -p "$CACHE_DIR"

    for i in yakitori ippin speed gohan drink fair; do
        curl "$CURL_OPTS" "${BASE_URL}/${i}/" -o "${CACHE_DIR}/${i}.html"
    done

    echo "Done!" 1>&2
}

help() {
    cat << EOF
Usage: torikizoku [ -c ] [ -n number ] [ -m menu ] [ -da ] [ -h ]

Options:
  -c
    Cache html files to cache directory. If cache is not found, it will request
    html file every time when the command runs. It is recommended to run with
    this option first, before using. It will store cache to
    \$HOME/.cache/torikizoku.
  -n <number>
    Displays <number> of items. By default, it is set to 1.
  -m <menu>
    Sets <menu> as menu type to display. Default value is yakitori. Value can
    be one of the followings; yakitori ippin speed gohan drink fair.
  -d
    Displays description of the given menu type. It will display menu and its
    desciption, separated by comma.
  -a
    Displays all menus of given menu type. If -d option is used, it will also
    display description too.
  -h
    Displays help message.
EOF
}

check_menu() {
    n=0

    for i in $@; do
        [ $n -eq 0 ] && n=$((n + 1)) && continue

        [ "$1" = "$i" ] && return 0

        n=$((n + 1))
    done

    echo "torikizoku: $1 is not valid menu type" 1>&2

    return 1
}


# default settings
num=1
menu="yakitori"
desc="false"
all="false"

while getopts "cn:m:dah" OPT; do
    case "$OPT" in
        c) cache && exit ;;
        n) num="$OPTARG" ;;
        m) menu="$OPTARG" ;;
        d) desc="true" ;;
        a) all="true" ;;
        h) help && exit ;;
    esac
done

check_menu "$menu" "yakitori" "ippin" "speed" "gohan" "drink" "fair" || exit 1

dep_check "curl" "pup" || exit 1

if [ -f "${CACHE_DIR}/${menu}.html" ]; then
    src=$(cat ${CACHE_DIR}/${menu}.html)
else
    src=$(curl "$CURL_OPTS" "${BASE_URL}/${menu}/")
fi

# menu
m=$(echo $src |
    pup "h4.menu__list__title, h4.menu__list__container__items__title, p.drink__list__flex__box__title" |
    grep -A 1 "menu__list__title\|menu__list__container__items__title\|drink__list__flex__box__title" |
    grep -v "\-\-\|class=" |
    tr -d " ")

# description
d=$(echo $src |
    pup "div.menu__list__text, div.menu__list__container__items__text, p.menu__list__container__items__text" |
    grep -A 1 "menu__list__text\|menu__list__container__items__text\|menu__list__container__items__text" |
    grep -v "\-\-\|class=" |
    tr -d " ")

# create csv from menu and description file
echo "$m" > "${TMP_FILE}.menu"
echo "$d" > "${TMP_FILE}.desc"

paste -d "," "${TMP_FILE}.menu" "${TMP_FILE}.desc" > "${TMP_FILE}.csv"

if [ "$desc" = "false" ]; then
    if [ "$all" = "false" ]; then
        cut -d "," -f 1 "${TMP_FILE}.csv" | shuf -n "$num"
    else
        cut -d "," -f 1 "${TMP_FILE}.csv"
    fi
else
    if [ "$all" = "false" ]; then
        shuf -n "$num" "${TMP_FILE}.csv"
    else
        cat "${TMP_FILE}.csv"
    fi
fi

# clean tmp files
rm -f "${TMP_FILE}.menu" "${TMP_FILE}.desc" "${TMP_FILE}.csv"
