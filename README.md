# torikizoku

[鳥貴族のメニュー](https://torikizoku.co.jp/menu/)
をランダムに表示するコマンドです。

## Requirements

- curl
- [pup](https://github.com/ericchiang/pup)

## Installation

```bash
sudo make install
torikizoku -c # キャッシュを保存する
```

## Usage

```
$ torikizoku [ -c ] [ -n number ] [ -m menu ] [ -da ] [ -h ]
```

### Options

- `-c` メニューのhtmlファイルをキャッシュする。

すべてのメニューのhtmlファイルをキャッシュとして保存します。
キャッシュは`$HOME/.cache/torikizoku`に保存されます。
キャッシュがない場合はコマンドを実行するたびにメニューを`curl`で取得します。
なのでインストール時に`torikizoku -c`と実行してキャッシュを保存することをおすすめします。

- `-n <number>` 表示するメニューの数を`<number>`に変更する。

```
$ torikizoku -n 3
むね貴族焼(スパイス)
やげんなんこつ
かわ塩
```

- `-m <menu>` 表示するメニューを`<menu>`に変更する。

指定したメニューの種類を表示します。
メニューは`yakitori ippin speed gohan drink fair`のどれかを指定してください。
デフォルトでは`yakitori`になっています。

```
$ torikizoku -m drink # drinkのメニューを表示
大隅焼酎ソーダ
```

- `-d` メニューの説明を表示します。

カンマ区切りでメニューとその説明を表示します。

```
$ torikizoku
ハート（ハツ）塩
$ torikizoku -d
砂ずり（砂肝）,食感が楽しい!!コリコリと噛む心地よさを引き立てる。
```

- `-a` 指定したメニューの種類をすべて表示します。

```
$ torikizoku -a
もも貴族焼(たれ)
もも貴族焼(塩)
もも貴族焼(スパイス)
むね貴族焼(たれ)
むね貴族焼(塩)
むね貴族焼(スパイス)
ちからこぶ塩
手羽先塩
三角(ぼんじり)
つくね塩
... # 長いので省略
$ torikizoku -a -m drink | grep ジュース | shuf -n 1
オレンジジュース
```

- `-h` ヘルプを表示します。

## Examples

たくさん飲みたい時

```
$ echo よーし今日は$(torikizoku -m drink)をたくさん飲んじゃうぞ〜
よーし今日はカルピスチューハイをたくさん飲んじゃうぞ〜
```

共食い

```
$ cowsay -f cock $(torikizoku -a | grep 貴族 | shuf -n 1)食いたい
 ________________________ 
< むね貴族焼(塩)食いたい > 
 ------------------------ 
    \ 
     \  /\/\ 
       \   / 
       |  0 >> 
       |___| 
 __((_<|   | 
(          | 
(__________) 
   |      | 
   |      | 
   /\     /\ 
```

何を注文するか考えなくたい時

```
$ echo-sd --stress $(torikizoku -n 5)
やげんなんこつ
　　　　↘
　　　ちからこぶたれ
　　　　↙
ちからこぶ塩
　　　　↘
　　　手羽先たれ
　　　　↙
＿人人人人人人人人＿
＞　ひざなんこつ　＜
￣Y^Y^Y^Y^Y^Y^Y^Y^￣
```
