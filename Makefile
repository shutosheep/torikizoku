PREFIX = /usr/local

install:
	mkdir -p ${PREFIX}/bin
	cp -f torikizoku ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/torikizoku

uninstall:
	rm -f ${PREFIX}/bin/torikizoku

.PHONY: install uninstall
